-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 12 Juin 2021 à 18:30
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fernandes_lucas_info1b_gestion_de_pret_104_2021`
--
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS fernandes_lucas_info1b_gestion_de_pret_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS fernandes_lucas_info1b_gestion_de_pret_104_2021;

-- Utilisation de cette base de donnée

USE fernandes_lucas_info1b_gestion_de_pret_104_2021;



-- --------------------------------------------------------

--
-- Structure de la table `t_employer`
--

CREATE TABLE `t_employer` (
  `ID_employer` int(11) NOT NULL,
  `Nom_employer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_employer`
--

INSERT INTO `t_employer` (`ID_employer`, `Nom_employer`) VALUES
(2, 'Omari Laager'),
(3, 'Alexandre Quirighetti'),
(4, 'Alvarez Brandon '),
(5, 'Giovanne Ruggero'),
(6, 'Tom Rey'),
(7, 'Fernando Costas'),
(21, 'lucas fernandes');

-- --------------------------------------------------------

--
-- Structure de la table `t_emprunter_materiel_personne`
--

CREATE TABLE `t_emprunter_materiel_personne` (
  `ID_emprunter_materiel_personne` int(11) NOT NULL,
  `FK_Employer` int(20) NOT NULL,
  `FK_Personne` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_emprunter_materiel_personne`
--

INSERT INTO `t_emprunter_materiel_personne` (`ID_emprunter_materiel_personne`, `FK_Employer`, `FK_Personne`) VALUES
(3, 6, 3),
(10, 2, 1),
(12, 3, 3),
(13, 7, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `ID_Personne` int(11) NOT NULL,
  `Nom_Personne` varchar(20) NOT NULL,
  `Prenom_Personne` varchar(20) NOT NULL,
  `Telephone_Personne` int(12) NOT NULL,
  `Mail_Personne` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`ID_Personne`, `Nom_Personne`, `Prenom_Personne`, `Telephone_Personne`, `Mail_Personne`) VALUES
(1, 'Scofield', 'Mickaël', 795866421, 'Naruto.hokage7329@gmail.com'),
(3, 'Hidolft', 'Mathis', 212556421, 'Mathis.promail@gmail.com'),
(4, 'Lopez', 'Pedro', 213245668, 'Lopez.ELPedro@hotmail.com');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_employer`
--
ALTER TABLE `t_employer`
  ADD PRIMARY KEY (`ID_employer`);

--
-- Index pour la table `t_emprunter_materiel_personne`
--
ALTER TABLE `t_emprunter_materiel_personne`
  ADD PRIMARY KEY (`ID_emprunter_materiel_personne`),
  ADD KEY `FK_Employer` (`FK_Employer`),
  ADD KEY `FK_Personne` (`FK_Personne`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`ID_Personne`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_employer`
--
ALTER TABLE `t_employer`
  MODIFY `ID_employer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `t_emprunter_materiel_personne`
--
ALTER TABLE `t_emprunter_materiel_personne`
  MODIFY `ID_emprunter_materiel_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `ID_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_emprunter_materiel_personne`
--
ALTER TABLE `t_emprunter_materiel_personne`
  ADD CONSTRAINT `t_emprunter_materiel_personne_ibfk_1` FOREIGN KEY (`FK_Personne`) REFERENCES `t_personne` (`ID_Personne`),
  ADD CONSTRAINT `t_emprunter_materiel_personne_ibfk_2` FOREIGN KEY (`FK_Employer`) REFERENCES `t_employer` (`ID_employer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
