"""
    Fichier : gestion_personne_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterPersonne(FlaskForm):
    """
        Dans le formulaire "emprunter_materiel_personne_ajouter.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_personne_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_personne_wtf = StringField("Ajouter le nom :",
                                   validators=[Length(min=2, max=20, message="min 2 max 20"),
                                               Regexp(nom_personne_regexp,
                                                      message="Pas de chiffres, de caractères "
                                                              "spéciaux, "
                                                              "d'espace à double, de double "
                                                              "apostrophe, de double trait union")
                                               ])

    prenom_personne_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_personne_wtf = StringField("Ajouter le prenom :",
                                   validators=[Length(min=2, max=20, message="min 2 max 20"),
                                               Regexp(prenom_personne_regexp,
                                                      message="Pas de chiffres, de caractères "
                                                              "spéciaux, "
                                                              "d'espace à double, de double "
                                                              "apostrophe, de double trait union")
                                               ])

    telephone_personne_regexp = "^[0-9]+$"
    telephone_personne_wtf = StringField("Ajouter le numéro de téléphone :",
                                         validators=[Length(min=10, max=12, message="min 10 max 12"),
                                                  Regexp(telephone_personne_regexp,
                                                         message="Mettre un numéro de téléphone avec les chiffre de 0-9")
                                                  ])

    mail_personne_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    mail_personne_wtf = StringField("Ajouter le mail :",
                                      validators=[Length(min=9, max=30, message="min 9 max 30"),
                                                  Regexp(mail_personne_regexp,
                                                         message="Pas de chiffres, de caractères "
                                                                 "spéciaux, "
                                                                 "d'espace à double, de double "
                                                                 "apostrophe, de double trait union")
                                                  ])

    submit = SubmitField("Enregistrer la personne")


class FormWTFUpdatePersonne(FlaskForm):
    """
        Dans le formulaire "personne_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_personne_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_personne_update_wtf = StringField("Éditer le nom suivant :",
                                          validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                      Regexp(nom_personne_update_regexp,
                                                             message="Pas de chiffres, de "
                                                                     "caractères "
                                                                     "spéciaux, "
                                                                     "d'espace à double, de double "
                                                                     "apostrophe, de double trait "
                                                                     "union")
                                                      ])

    prenom_personne_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_personne_update_wtf = StringField("Éditer le prénom suivant :",
                                          validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                      Regexp(prenom_personne_update_regexp,
                                                             message="Pas de chiffres, de "
                                                                     "caractères "
                                                                     "spéciaux, "
                                                                     "d'espace à double, de double "
                                                                     "apostrophe, de double trait "
                                                                     "union")
                                                      ])

    telephone_personne_update_regexp = "^[0-9]+$"
    telephone_personne_update_wtf = StringField("Éditer le téléphone suivant :",
                                             validators=[Length(min=10, max=12, message="min 10 max 12"),
                                                         Regexp(telephone_personne_update_regexp,
                                                                message="Modifier votre numéro de téléphone avec des chiffres de 0-9")
                                                         ])

    mail_personne_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    mail_personne_update_wtf = StringField("Éditer le mail suivant :",
                                             validators=[Length(min=9, max=40, message="min 9 max 40"),
                                                         Regexp(mail_personne_update_regexp,
                                                                message="Pas de chiffres, de "
                                                                        "caractères "
                                                                        "spéciaux, "
                                                                        "d'espace à double, de double "
                                                                        "apostrophe, de double trait "
                                                                        "union")
                                                         ])

    submit = SubmitField("Update personne")


class FormWTFDeletePersonne(FlaskForm):
    """
        Dans le formulaire "personne_delete_wtf.html"

        nom_personne_delete_wtf : Champ qui reçoit la valeur du employer, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "personne".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_personne".
    """
    nom_personne_delete_wtf = StringField("Effacer la personne suivant :")
    submit_btn_del = SubmitField("Effacer personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
