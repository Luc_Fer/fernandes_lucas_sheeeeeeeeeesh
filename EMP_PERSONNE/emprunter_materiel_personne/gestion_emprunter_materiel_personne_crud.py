"""
    Fichier : personne_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from EMP_PERSONNE import obj_mon_application
from EMP_PERSONNE.database.connect_db_context_manager import MaBaseDeDonnee
from EMP_PERSONNE.erreurs.exceptions import *
from EMP_PERSONNE.erreurs.msg_erreurs import *
from EMP_PERSONNE.Personne.gestion_personne_wtf_forms import FormWTFAjouterPersonne
from EMP_PERSONNE.Personne.gestion_personne_wtf_forms import FormWTFUpdatePersonne
from EMP_PERSONNE.Personne.gestion_personne_wtf_forms import FormWTFDeletePersonne

"""
    Nom : personne_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /personne_afficher
    
    But : Afficher les films avec les genres associés pour chaque film.
    
    Paramètres : id_genre_sel = 0 >> tous les films.
                 id_genre_sel = "n" affiche le film dont l'id est "n"
                 
"""


@obj_mon_application.route("/personne_afficher/<int:id_personne_sel>", methods=['GET', 'POST'])
def personne_afficher(id_personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_personne_afficher:
                code, msg = Exception_init_personne_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_personne_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_personne_afficher.args[0]} , "
                      f"{Exception_init_personne_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_personne_afficher_data = """SELECT id_Personne, Nom_Personne, Prenom_Personne, Telephone_Personne, Mail_Personne,
                                                            GROUP_CONCAT(Nom_employer) as EmprunterMaterielPersonne FROM t_emprunter_materiel_personne
                                                            RIGHT JOIN t_personne ON t_personne.id_Personne = t_emprunter_materiel_personne.fk_Personne
                                                            LEFT JOIN t_employer ON t_employer.id_employer = t_emprunter_materiel_personne.fk_Employer
                                                            GROUP BY id_Personne"""
                if id_personne_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_personne_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_personne_afficher_data += """ HAVING id_personne= %(value_id_personne_selected)s"""

                    mc_afficher.execute(strsql_personne_afficher_data, valeur_id_personne_selected_dictionnaire)

                # Récupère les données de la requête.
                data_personne_afficher = mc_afficher.fetchall()
                print("data_employés ", data_personne_afficher, " Type : ", type(data_personne_afficher))

                # Différencier les messages.
                if not data_personne_afficher and id_personne_sel == 0:
                    flash("""La table "t_personne" est vide. !""", "warning")
                elif not data_personne_afficher and id_personne_sel > 0:
                    # Si l'utilisateur change l'id_personne dans l'URL et qu'il ne correspond à aucun film
                    flash(f"Le film {id_personne_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données personnes et employés affichés !!", "success")

        except Exception as Exception_personne_afficher:
            code, msg = Exception_personne_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception personne_afficher : {sys.exc_info()[0]} "
                  f"{Exception_personne_afficher.args[0]} , "
                  f"{Exception_personne_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("Preparer_Personne/personne_afficher.html", data=data_personne_afficher)


"""
    nom: edit_genre_film_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "personne_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les genres contenus dans la "t_genre".
    2) Les genres attribués au film selectionné.
    3) Les genres non-attribués au film sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_emprunter_materiel_personne_selected", methods=['GET', 'POST'])
def edit_emprunter_materiel_personne_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_employés_afficher = """SELECT id_employer, Nom_employer FROM t_employer ORDER BY id_employer ASC"""
                mc_afficher.execute(strsql_employés_afficher)
            data_employés_all = mc_afficher.fetchall()
            print("dans edit_emprunter_materiel_personne_selected ---> data_employés_all", data_employés_all)

            # Récupère la valeur de "id_film" du formulaire html "personne_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "id_film_genres_edit_html" dans le fichier "personne_afficher.html"
            # href="{{ url_for('edit_genre_film_selected', id_film_genres_edit_html=row.id_Personne) }}"
            id_emprunter_materiel_personne_edit = request.values['id_emprunter_materiel_personne_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_emprunter_materiel_personne_edit'] = id_emprunter_materiel_personne_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_emprunter_materiel_personne_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction personne_afficher_data
            # 1) Sélection du film choisi
            # 2) Sélection des genres "déjà" attribués pour le film.
            # 3) Sélection des genres "pas encore" attribués pour le film choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "personne_afficher_data"
            data_emprunter_materiel_personne_selected, data_emprunter_materiel_personne_non_attribues, data_emprunter_materiel_personne_attribues = \
                personne_afficher_data(valeur_id_personne_selected_dictionnaire)

            print(data_emprunter_materiel_personne_selected)
            lst_data_personne_selected = [item['id_Personne'] for item in data_emprunter_materiel_personne_selected]
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les employés qui ne sont pas encore sélectionnés.
            lst_data_emprunter_materiel_personne_non_attribues = [item['id_employer'] for item in data_emprunter_materiel_personne_non_attribues]
            session['session_lst_data_emprunter_materiel_personne_non_attribues'] = lst_data_emprunter_materiel_personne_non_attribues
            print("lst_data_emprunter_materiel_personne_non_attribues  ", lst_data_emprunter_materiel_personne_non_attribues,
                  type(lst_data_emprunter_materiel_personne_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les employés qui sont déjà sélectionnés.
            lst_data_emprunter_materiel_personne_old_attribues = [item['id_employer'] for item in data_emprunter_materiel_personne_attribues]
            session['session_lst_data_emprunter_materiel_personne_old_attribues'] = lst_data_emprunter_materiel_personne_old_attribues
            print("lst_data_emprunter_materiel_personne_old_attribues  ", lst_data_emprunter_materiel_personne_old_attribues,
                  type(lst_data_emprunter_materiel_personne_old_attribues))

            print(" data data_emprunter_materiel_personne_selected", data_emprunter_materiel_personne_selected, "type ", type(data_emprunter_materiel_personne_selected))
            print(" data data_emprunter_materiel_personne_non_attribues ", data_emprunter_materiel_personne_non_attribues, "type ",
                  type(data_emprunter_materiel_personne_non_attribues))
            print(" data_emprunter_materiel_personne_attribues ", data_emprunter_materiel_personne_attribues, "type ",
                  type(data_emprunter_materiel_personne_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_emprunter_materiel_personne_non_attribues = [item['Nom_employer'] for item in data_emprunter_materiel_personne_non_attribues]
            print("lst_all_employés gf_edit_emprunter_materiel_personne_selected ", lst_data_emprunter_materiel_personne_non_attribues,
                  type(lst_data_emprunter_materiel_personne_non_attribues))

        except Exception as Exception_edit_emprunter_materiel_personne_selected:
            code, msg = Exception_edit_emprunter_materiel_personne_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_emprunter_materiel_personne_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_emprunter_materiel_personne_selected.args[0]} , "
                  f"{Exception_edit_emprunter_materiel_personne_selected}", "danger")

    return render_template("emprunter_materiel_personne/emprunter_materiel_personne_modifier_tags_dropbox.html",
                           data_employés=data_employés_all,
                           data_personne_selected=data_emprunter_materiel_personne_selected,
                           data_employés_attribues=data_emprunter_materiel_personne_attribues,
                           data_employés_non_attribues=data_emprunter_materiel_personne_non_attribues)


"""
    nom: update_genre_film_selected

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "personne_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les genres contenus dans la "t_genre".
    2) Les genres attribués au film selectionné.
    3) Les genres non-attribués au film sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_emprunter_materiel_personne_selected", methods=['GET', 'POST'])
def update_emprunter_materiel_personne_selected():
    if request.method == "POST":
        try:
            # Récupère l'id de la personne sélectionné
            id_personne_selected = session['session_id_emprunter_materiel_personne_edit']
            print("session['session_id_emprunter_materiel_personne_edit'] ", session['session_id_emprunter_materiel_personne_edit'])

            # Récupère la liste des genres qui ne sont pas associés au film sélectionné.
            old_lst_data_emprunter_materiel_personne_non_attribues = session['session_lst_data_emprunter_materiel_personne_non_attribues']
            print("old_lst_data_emprunter_materiel_personne_non_attribues ", old_lst_data_emprunter_materiel_personne_non_attribues)

            # Récupère la liste des genres qui sont associés au film sélectionné.
            old_lst_data_emprunter_materiel_personne_attribues = session['session_lst_data_emprunter_materiel_personne_old_attribues']
            print("old_lst_data_emprunter_materiel_personne_old_attribues ", old_lst_data_emprunter_materiel_personne_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme genres dans le composant "tags-selector-tagselect"
            # dans le fichier "genres_films_modifier_tags_dropbox.html"
            new_lst_str_emprunter_materiel_personne = request.form.getlist('name_select_tags')
            print("new_lst_str_emprunter_materiel_personne ", new_lst_str_emprunter_materiel_personne)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_emprunter_materiel_personne_old = list(map(int, new_lst_str_emprunter_materiel_personne))
            print("new_lst_emprunter_materiel_personne ", new_lst_int_emprunter_materiel_personne_old, "type new_lst_emprunter_materiel_personne ",
                  type(new_lst_int_emprunter_materiel_personne_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genre_film".
            lst_diff_employés_delete_b = list(
                set(old_lst_data_emprunter_materiel_personne_attribues) - set(new_lst_int_emprunter_materiel_personne_old))
            print("lst_diff_employés_delete_b ", lst_diff_employés_delete_b)

            # Une liste de "id_genre" qui doivent être ajoutés à la "t_genre_film"
            lst_diff_employés_insert_a = list(
                set(new_lst_int_emprunter_materiel_personne_old) - set(old_lst_data_emprunter_materiel_personne_attribues))
            print("lst_diff_employés_insert_a ", lst_diff_employés_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_Personne"/"id_Personne" et "fk_Employer"/"id_employer" dans la "t_emprunter_materiel_personne"
            strsql_insert_emprunter_materiel_personne = """INSERT INTO t_emprunter_materiel_personne (id_emprunter_materiel_personne, fk_Employer, fk_Personne) VALUES (NULL, %(value_fk_Employer)s, %(value_fk_Personne)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_emprunter_materiel_personne = """DELETE FROM t_emprunter_materiel_personne WHERE fk_Employer = %(value_fk_Employer)s AND fk_Personne = %(value_fk_Personne)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le film sélectionné, parcourir la liste des genres à INSÉRER dans la "t_emprunter_materiel_personne".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_employer_ins in lst_diff_employés_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
                    valeurs_personne_sel_employer_sel_dictionnaire = {"value_fk_Personne": id_personne_selected,
                                                                      "value_fk_Employer": id_employer_ins}

                    mconn_bd.mabd_execute(strsql_insert_emprunter_materiel_personne, valeurs_personne_sel_employer_sel_dictionnaire)

                # Pour le film sélectionné, parcourir la liste des genres à EFFACER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_employer_del in lst_diff_employés_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "id_genre_del" (l'id du genre dans la liste) associé à une variable.
                    valeurs_personne_sel_employer_sel_dictionnaire = {"value_fk_Personne": id_personne_selected,
                                                               "value_fk_Employer": id_employer_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_emprunter_materiel_personne, valeurs_personne_sel_employer_sel_dictionnaire)

        except Exception as Exception_update_emprunter_materiel_personne_selected:
            code, msg = Exception_update_emprunter_materiel_personne_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_emprunter_materiel_personne_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_emprunter_materiel_personne_selected.args[0]} , "
                  f"{Exception_update_emprunter_materiel_personne_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_emprunter_materiel_personne",
    # on affiche les personnes et le(urs) employer(s) associé(s).
    return redirect(url_for('personne_afficher', id_personne_sel=id_personne_selected))


"""
    nom: personne_afficher_data

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "personne_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des genres, ainsi l'utilisateur voit les genres à disposition

    On signale les erreurs importantes
"""


def personne_afficher_data(valeur_id_personne_selected_dict):
    print("valeur_id_personne_selected_dict...", valeur_id_personne_selected_dict)
    try:

        strsql_personne_selected = """SELECT id_Personne, Nom_Personne, Prenom_Personne, Telephone_Personne, Mail_Personne, GROUP_CONCAT(id_employer) as EmprunterMaterielPersonne FROM t_emprunter_materiel_personne
                                        INNER JOIN t_personne ON t_personne.id_Personne = t_emprunter_materiel_personne.fk_Personne
                                        INNER JOIN t_employer ON t_employer.id_employer = t_emprunter_materiel_personne.fk_Employer
                                        WHERE id_Personne = %(value_id_personne_selected)s"""

        strsql_emprunter_materiel_personne_non_attribues = """SELECT id_employer, Nom_employer FROM t_employer WHERE id_employer not in(SELECT id_employer as idEmprunterMaterielPersonne FROM t_emprunter_materiel_personne
                                                    INNER JOIN t_personne ON t_personne.id_Personne = t_emprunter_materiel_personne.fk_Personne
                                                    INNER JOIN t_employer ON t_employer.id_employer = t_emprunter_materiel_personne.fk_Employer
                                                    WHERE id_Personne = %(value_id_personne_selected)s)"""

        strsql_emprunter_materiel_personne_attribues = """SELECT id_Personne, id_employer, Nom_employer FROM t_emprunter_materiel_personne
                                            INNER JOIN t_personne ON t_personne.id_Personne = t_emprunter_materiel_personne.fk_Personne
                                            INNER JOIN t_employer ON t_employer.id_employer = t_emprunter_materiel_personne.fk_Employer
                                            WHERE id_Personne = %(value_id_personne_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_emprunter_materiel_personne_non_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_emprunter_materiel_personne_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("personne_afficher_data ----> data_emprunter_materiel_personne_non_attribues ", data_emprunter_materiel_personne_non_attribues,
                  " Type : ",
                  type(data_emprunter_materiel_personne_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_personne_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_emprunter_materiel_personne_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_emprunter_materiel_personne_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_emprunter_materiel_personne_attribues ", data_emprunter_materiel_personne_attribues, " Type : ",
                  type(data_emprunter_materiel_personne_attribues))

            # Retourne les données des "SELECT"
            return data_personne_selected, data_emprunter_materiel_personne_non_attribues, data_emprunter_materiel_personne_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans personne_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans personne_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_personne_afficher_data:
        code, msg = IntegrityError_personne_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans personne_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_personne_afficher_data.args[0]} , "
              f"{IntegrityError_personne_afficher_data}", "danger")


@obj_mon_application.route("/personne_ajouter", methods=['GET', 'POST'])
def personne_ajouter_wtf():
    form = FormWTFAjouterPersonne()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion employés ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionEmployés {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                nom_personne_wtf = form.nom_personne_wtf.data
                nom_personne = nom_personne_wtf.lower()

                prenom_personne_wtf = form.prenom_personne_wtf.data
                prenom_personne = prenom_personne_wtf.lower()

                telephone_personne_wtf = form.telephone_personne_wtf.data
                telephone_personne = telephone_personne_wtf.lower()

                mail_personne_wtf = form.mail_personne_wtf.data
                mail_personne = mail_personne_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_nom_personne": nom_personne, "value_prenom_personne": prenom_personne, "value_telephone_personne": telephone_personne, "value_mail_personne": mail_personne}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_personne = """INSERT INTO t_personne (id_Personne, Nom_Personne, Prenom_Personne, Telephone_Personne, Mail_Personne) VALUES (NULL, %(value_nom_personne)s, %(value_prenom_personne)s, %(value_telephone_personne)s, %(value_mail_personne)s)"""

                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_personne, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('personne_afficher', order_by='DESC', id_personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personne_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personne_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion employés CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("Preparer_Personne/personne_ajouter_wtf.html", form=form)


@obj_mon_application.route("/personne_update", methods=['GET', 'POST'])
def personne_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_employer"
    id_personne_update = request.values['id_personne_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatePersonne()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "personne_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            nom_personne_update = form_update.nom_personne_update_wtf.data
            nom_personne_update = nom_personne_update.lower()

            prenom_personne_update = form_update.prenom_personne_update_wtf.data
            prenom_personne_update = prenom_personne_update.lower()

            telephone_personne_update = form_update.telephone_personne_update_wtf.data
            telephone_personne_update = telephone_personne_update.lower()

            mail_personne_update = form_update.mail_personne_update_wtf.data
            mail_personne_update = mail_personne_update.lower()

            valeur_update_dictionnaire = {"value_id_personne": id_personne_update, "value_nom_personne": nom_personne_update, "value_prenom_personne": prenom_personne_update, "value_telephone_personne": telephone_personne_update, "value_mail_personne": mail_personne_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulepersonne = """UPDATE t_personne SET Nom_Personne = %(value_nom_personne)s, Prenom_Personne = %(value_prenom_personne)s, telephone_personne = %(value_telephone_personne)s, mail_personne = %(value_mail_personne)s WHERE id_Personne = %(value_id_personne)s """
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulepersonne, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_employer_update"
            return redirect(url_for('personne_afficher', order_by="ASC", id_personne_sel=id_personne_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_employer" et "Nom_employer" de la "t_employer"
            str_sql_id_personne = "SELECT * FROM t_personne WHERE id_Personne = %(value_id_personne)s "
            valeur_select_dictionnaire = {"value_id_personne": id_personne_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_personne, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom personne" pour l'UPDATE
            data_nom_personne = mybd_curseur.fetchone()

            print("data_nom_employer ", data_nom_personne, " type ", type(data_nom_personne), " personne ",
                  data_nom_personne["Nom_Personne"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personne_update_wtf.html"
            form_update.nom_personne_update_wtf.data = data_nom_personne["Nom_Personne"]
            form_update.prenom_personne_update_wtf.data = data_nom_personne["Prenom_Personne"]
            form_update.telephone_personne_update_wtf.data = data_nom_personne["Telephone_Personne"]
            form_update.mail_personne_update_wtf.data = data_nom_personne["Mail_Personne"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans personne_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans personne_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans personne_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans personne_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Preparer_Personne/personne_update_wtf.html", form_update=form_update)


@obj_mon_application.route("/personne_delete", methods=['GET', 'POST'])
def personne_delete_wtf():
    data_personnes_attribue_employer_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_Personne"
    id_personne_delete = request.values['id_personne_btn_delete_html']

    # Objet formulaire pour effacer le employer sélectionné.
    form_delete = FormWTFDeletePersonne()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("personne_afficher", order_by="ASC", id_personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau le formulaire "Preparer_Personne/personne_delete_wtf.html"
                # lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_personnes_attribue_employer_delete = session['data_personnes_attribue_employer_delete']
                print("data_personnes_attribue_employer_delete ", data_personnes_attribue_employer_delete)

                flash(f"Effacer la personne de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer employer" qui va irrémédiablement EFFACER la personne
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_emprunter_materiel_personne = """DELETE FROM t_emprunter_materiel_personne WHERE fk_Personne = %(value_id_personne)s """
                str_sql_delete_idemployer = """DELETE FROM t_personne WHERE id_Personne = %(value_id_personne)s"""
                # Manière brutale d'effacer d'abord la "fk_Employer", même si elle n'existe pas dans la
                # "t_emprunter_materiel_personne" Ensuite on peut effacer la personne vu qu'il n'est plus "lié" (INNODB) dans la
                # "t_emprunter_materiel_personne"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_emprunter_materiel_personne, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idemployer, valeur_delete_dictionnaire)

                flash(f"Personne définitivement effacé !!", "success")
                print(f"Personne définitivement effacé !!")

                # afficher les données
                return redirect(url_for('personne_afficher', order_by="ASC", id_personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_personne": id_personne_delete}
            print(id_personne_delete, type(id_personne_delete))

            # Requête qui affiche tous les films qui ont le employer que l'utilisateur veut effacer
            str_sql_emprunter_materiel_personne_delete = """SELECT id_emprunter_materiel_personne, Nom_employer, id_Personne, Nom_Personne, Prenom_Personne, Telephone_Personne, Mail_Personne FROM t_emprunter_materiel_personne 
                                                            INNER JOIN t_employer ON t_emprunter_materiel_personne.fk_Employer = t_employer.id_employer
                                                            INNER JOIN t_personne ON t_emprunter_materiel_personne.fk_Personne = t_personne.id_Personne
                                                            WHERE fk_Personne = %(value_id_personne)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_emprunter_materiel_personne_delete, valeur_select_dictionnaire)
            data_personnes_attribue_employer_delete = mybd_curseur.fetchall()
            print("data_personnes_attribue_employer_delete...", data_personnes_attribue_employer_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau le formulaire
            # "Preparer_Personne/personne_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_personnes_attribue_employer_delete'] = data_personnes_attribue_employer_delete

            # Opération sur la BD pour récupérer "id_employer" et "Nom_employer" de la "t_employer"
            str_sql_id_personne = "SELECT id_Personne, Nom_Personne, Prenom_Personne, Telephone_Personne, Mail_Personne FROM t_personne WHERE id_Personne = %(value_id_personne)s "

            mybd_curseur.execute(str_sql_id_personne, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom employer" pour l'action DELETE
            data_nom_personne = mybd_curseur.fetchone()
            print("data_nom_personne", data_nom_personne, " type ", type(data_nom_personne), " personne ",
                  data_nom_personne["Nom_Personne"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personne_delete_wtf.html"
            form_delete.nom_personne_delete_wtf.data = data_nom_personne["Nom_Personne"]

            # Le bouton pour l'action "DELETE" dans le form. "personne_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans personne_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans personne_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans personne_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans personne_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Preparer_Personne/personne_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_personnes_associes=data_personnes_attribue_employer_delete)
